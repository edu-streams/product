import { Box, ToggleButton, ToggleButtonGroup, Container } from "@mui/material";
import { useEffect, useState } from "react";
import Typography from "@mui/material/Typography";
import { socket } from "../../services/socket";

const OBSControl = () => {
  const [isDisplay, setIsDisplay] = useState(false);

  const handleChange = (event) => {
    if (event.target.value === "false") {
      setIsDisplay(false);
    }
    if (event.target.value === "true") {
      setIsDisplay(true);
    }
  };

  useEffect(() => {
    // Якщо потрібно при вмиканні віджету надсилати якісь додаткові дані окрім просто запуску віджета - дописувати їх у об'єкт
    const resultOptions = { isDisplay };

    socket.emit("control", resultOptions);
  }, [isDisplay]);

  return (
    <>
      <Typography>OBS Control</Typography>
      <Box sx={{display: "flex", justifyContent: "center"}}>
        <ToggleButtonGroup
          color="primary"
          value={isDisplay}
          exclusive
          onChange={handleChange}
          aria-label="Platform"
        >
          <ToggleButton disabled={isDisplay} value={true}>
            Enable
          </ToggleButton>
          <ToggleButton disabled={!isDisplay} value={false}>
            Disable
          </ToggleButton>
        </ToggleButtonGroup>
      </Box>
    </>
  );
};
export default OBSControl;
